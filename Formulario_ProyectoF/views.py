from django.shortcuts import render
from django.views import View
from .forms import BuscadorMamiferos

class Mamiferos(View):

    def rbm(request):

        BM=BuscadorMamiferos()

        datos={
            "form":BM
        }

        BuscadorM=render(request,"FormsMamiferos.html",datos)
        return BuscadorM
