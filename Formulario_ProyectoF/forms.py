from django.forms import CharField, Form

class BuscadorMamiferos(Form):
    Nombre=CharField(min_length=4 ,max_length=20)
    Color=CharField(min_length=4 ,max_length=20)
    Tamaño=CharField(min_length=4 ,max_length=10)
    Tipo=CharField(min_length=4 ,max_length=10)
